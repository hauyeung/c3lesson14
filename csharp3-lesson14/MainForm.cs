﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace csharp3_lesson14
{
    public partial class MainForm : Form
    {
        public MainForm()
        {            
            InitializeComponent();
            this.photoPictureBox.AllowDrop = true;
        }

        private void addressesBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.addressesBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.addressesDataSet);

        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'addressesDataSet.addresses' table. You can move, or remove it, as needed.
            this.addressesTableAdapter.Fill(this.addressesDataSet.addresses);

        }

        private void birthdayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Birthdays b = new Birthdays();
            b.ShowDialog();
        }

        private void notesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Notes n = new Notes();
            n.ShowDialog();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void photoPictureBox_DragDrop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] filePaths = (string[])e.Data.GetData(DataFormats.FileDrop);
                if (filePaths.Length > 0)
                {
                    // Attempt to load, may not be valid image
                    string path = filePaths[0];
                    try
                    {
                        using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream(System.IO.File.ReadAllBytes(path)))
                        {
                            Image tempImage = Image.FromStream(memoryStream);
                            photoPictureBox.Image = new Bitmap(tempImage);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error loading file: " + ex.Message);
                    }
                }


            }
            
        }

        private void MainForm_DragDrop(object sender, DragEventArgs e)
        {
            
        }

        private void photoPictureBox_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;
        }

        private void bindingNavigatorDeleteItem_Click(object sender, EventArgs e)
        {
           this.addressesBindingSource.EndEdit();
           this.tableAdapterManager.UpdateAll(this.addressesDataSet);
        }
    }
}
