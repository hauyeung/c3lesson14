﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace csharp3_lesson14
{
    public partial class Birthdays : Form
    {
        public Birthdays()
        {
            InitializeComponent();
        }

        private void addressesBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.addressesBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.addressesDataSet);

        }

        private void Birthdays_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'addressesDataSet.addresses' table. You can move, or remove it, as needed.
            this.addressesTableAdapter.Fill(this.addressesDataSet.addresses);

        }
    }
}
