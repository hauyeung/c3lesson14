﻿namespace csharp3_lesson14
{
    partial class Notes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label firstNameLabel;
            System.Windows.Forms.Label lastNameLabel;
            System.Windows.Forms.Label addressLabel;
            System.Windows.Forms.Label cityLabel;
            System.Windows.Forms.Label state_ProvinceLabel;
            System.Windows.Forms.Label zip_Postal_CodeLabel;
            System.Windows.Forms.Label countryLabel;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Notes));
            System.Windows.Forms.Label notesLabel1;
            this.notesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.addressesDataSet = new csharp3_lesson14.addressesDataSet();
            this.firstNameLabel1 = new System.Windows.Forms.Label();
            this.addressesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.lastNameLabel1 = new System.Windows.Forms.Label();
            this.addressLabel1 = new System.Windows.Forms.Label();
            this.cityLabel1 = new System.Windows.Forms.Label();
            this.state_ProvinceLabel1 = new System.Windows.Forms.Label();
            this.zip_Postal_CodeLabel1 = new System.Windows.Forms.Label();
            this.countryLabel1 = new System.Windows.Forms.Label();
            this.addressesTableAdapter = new csharp3_lesson14.addressesDataSetTableAdapters.addressesTableAdapter();
            this.tableAdapterManager = new csharp3_lesson14.addressesDataSetTableAdapters.TableAdapterManager();
            this.bindingNavigator1 = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.notesTextBox = new System.Windows.Forms.TextBox();
            this.savenotetoolStripButton = new System.Windows.Forms.ToolStripButton();
            firstNameLabel = new System.Windows.Forms.Label();
            lastNameLabel = new System.Windows.Forms.Label();
            addressLabel = new System.Windows.Forms.Label();
            cityLabel = new System.Windows.Forms.Label();
            state_ProvinceLabel = new System.Windows.Forms.Label();
            zip_Postal_CodeLabel = new System.Windows.Forms.Label();
            countryLabel = new System.Windows.Forms.Label();
            notesLabel1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.notesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressesDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).BeginInit();
            this.bindingNavigator1.SuspendLayout();
            this.SuspendLayout();
            // 
            // firstNameLabel
            // 
            firstNameLabel.AutoSize = true;
            firstNameLabel.Location = new System.Drawing.Point(38, 16);
            firstNameLabel.Name = "firstNameLabel";
            firstNameLabel.Size = new System.Drawing.Size(60, 13);
            firstNameLabel.TabIndex = 3;
            firstNameLabel.Text = "First Name:";
            // 
            // lastNameLabel
            // 
            lastNameLabel.AutoSize = true;
            lastNameLabel.Location = new System.Drawing.Point(249, 14);
            lastNameLabel.Name = "lastNameLabel";
            lastNameLabel.Size = new System.Drawing.Size(61, 13);
            lastNameLabel.TabIndex = 5;
            lastNameLabel.Text = "Last Name:";
            // 
            // addressLabel
            // 
            addressLabel.AutoSize = true;
            addressLabel.Location = new System.Drawing.Point(40, 50);
            addressLabel.Name = "addressLabel";
            addressLabel.Size = new System.Drawing.Size(48, 13);
            addressLabel.TabIndex = 7;
            addressLabel.Text = "Address:";
            // 
            // cityLabel
            // 
            cityLabel.AutoSize = true;
            cityLabel.Location = new System.Drawing.Point(256, 48);
            cityLabel.Name = "cityLabel";
            cityLabel.Size = new System.Drawing.Size(27, 13);
            cityLabel.TabIndex = 9;
            cityLabel.Text = "City:";
            // 
            // state_ProvinceLabel
            // 
            state_ProvinceLabel.AutoSize = true;
            state_ProvinceLabel.Location = new System.Drawing.Point(38, 82);
            state_ProvinceLabel.Name = "state_ProvinceLabel";
            state_ProvinceLabel.Size = new System.Drawing.Size(82, 13);
            state_ProvinceLabel.TabIndex = 11;
            state_ProvinceLabel.Text = "State/Province:";
            // 
            // zip_Postal_CodeLabel
            // 
            zip_Postal_CodeLabel.AutoSize = true;
            zip_Postal_CodeLabel.Location = new System.Drawing.Point(256, 82);
            zip_Postal_CodeLabel.Name = "zip_Postal_CodeLabel";
            zip_Postal_CodeLabel.Size = new System.Drawing.Size(87, 13);
            zip_Postal_CodeLabel.TabIndex = 13;
            zip_Postal_CodeLabel.Text = "Zip/Postal Code:";
            // 
            // countryLabel
            // 
            countryLabel.AutoSize = true;
            countryLabel.Location = new System.Drawing.Point(42, 120);
            countryLabel.Name = "countryLabel";
            countryLabel.Size = new System.Drawing.Size(46, 13);
            countryLabel.TabIndex = 15;
            countryLabel.Text = "Country:";
            // 
            // notesBindingSource
            // 
            this.notesBindingSource.DataMember = "notes";
            this.notesBindingSource.DataSource = this.addressesDataSet;
            // 
            // addressesDataSet
            // 
            this.addressesDataSet.DataSetName = "addressesDataSet";
            this.addressesDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // firstNameLabel1
            // 
            this.firstNameLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.addressesBindingSource, "FirstName", true));
            this.firstNameLabel1.Location = new System.Drawing.Point(104, 16);
            this.firstNameLabel1.Name = "firstNameLabel1";
            this.firstNameLabel1.Size = new System.Drawing.Size(100, 23);
            this.firstNameLabel1.TabIndex = 4;
            this.firstNameLabel1.Text = "label1";
            // 
            // addressesBindingSource
            // 
            this.addressesBindingSource.DataMember = "addresses";
            this.addressesBindingSource.DataSource = this.addressesDataSet;
            // 
            // lastNameLabel1
            // 
            this.lastNameLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.addressesBindingSource, "LastName", true));
            this.lastNameLabel1.Location = new System.Drawing.Point(316, 14);
            this.lastNameLabel1.Name = "lastNameLabel1";
            this.lastNameLabel1.Size = new System.Drawing.Size(100, 23);
            this.lastNameLabel1.TabIndex = 6;
            this.lastNameLabel1.Text = "label1";
            // 
            // addressLabel1
            // 
            this.addressLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.addressesBindingSource, "Address", true));
            this.addressLabel1.Location = new System.Drawing.Point(94, 50);
            this.addressLabel1.Name = "addressLabel1";
            this.addressLabel1.Size = new System.Drawing.Size(100, 23);
            this.addressLabel1.TabIndex = 8;
            this.addressLabel1.Text = "label1";
            // 
            // cityLabel1
            // 
            this.cityLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.addressesBindingSource, "City", true));
            this.cityLabel1.Location = new System.Drawing.Point(289, 48);
            this.cityLabel1.Name = "cityLabel1";
            this.cityLabel1.Size = new System.Drawing.Size(100, 23);
            this.cityLabel1.TabIndex = 10;
            this.cityLabel1.Text = "label1";
            // 
            // state_ProvinceLabel1
            // 
            this.state_ProvinceLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.addressesBindingSource, "State/Province", true));
            this.state_ProvinceLabel1.Location = new System.Drawing.Point(135, 82);
            this.state_ProvinceLabel1.Name = "state_ProvinceLabel1";
            this.state_ProvinceLabel1.Size = new System.Drawing.Size(100, 23);
            this.state_ProvinceLabel1.TabIndex = 12;
            this.state_ProvinceLabel1.Text = "label1";
            // 
            // zip_Postal_CodeLabel1
            // 
            this.zip_Postal_CodeLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.addressesBindingSource, "Zip/Postal Code", true));
            this.zip_Postal_CodeLabel1.Location = new System.Drawing.Point(372, 82);
            this.zip_Postal_CodeLabel1.Name = "zip_Postal_CodeLabel1";
            this.zip_Postal_CodeLabel1.Size = new System.Drawing.Size(100, 23);
            this.zip_Postal_CodeLabel1.TabIndex = 14;
            this.zip_Postal_CodeLabel1.Text = "label1";
            // 
            // countryLabel1
            // 
            this.countryLabel1.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.addressesBindingSource, "Country", true));
            this.countryLabel1.Location = new System.Drawing.Point(94, 120);
            this.countryLabel1.Name = "countryLabel1";
            this.countryLabel1.Size = new System.Drawing.Size(100, 23);
            this.countryLabel1.TabIndex = 16;
            this.countryLabel1.Text = "label1";
            // 
            // addressesTableAdapter
            // 
            this.addressesTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.addressesTableAdapter = this.addressesTableAdapter;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.UpdateOrder = csharp3_lesson14.addressesDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // bindingNavigator1
            // 
            this.bindingNavigator1.AddNewItem = this.bindingNavigatorAddNewItem;
            this.bindingNavigator1.BindingSource = this.addressesBindingSource;
            this.bindingNavigator1.CountItem = this.bindingNavigatorCountItem;
            this.bindingNavigator1.DeleteItem = this.bindingNavigatorDeleteItem;
            this.bindingNavigator1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bindingNavigator1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.savenotetoolStripButton});
            this.bindingNavigator1.Location = new System.Drawing.Point(0, 356);
            this.bindingNavigator1.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.bindingNavigator1.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.bindingNavigator1.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.bindingNavigator1.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.bindingNavigator1.Name = "bindingNavigator1";
            this.bindingNavigator1.PositionItem = this.bindingNavigatorPositionItem;
            this.bindingNavigator1.Size = new System.Drawing.Size(515, 25);
            this.bindingNavigator1.TabIndex = 17;
            this.bindingNavigator1.Text = "bindingNavigator1";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 22);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // notesLabel1
            // 
            notesLabel1.AutoSize = true;
            notesLabel1.Location = new System.Drawing.Point(42, 178);
            notesLabel1.Name = "notesLabel1";
            notesLabel1.Size = new System.Drawing.Size(38, 13);
            notesLabel1.TabIndex = 17;
            notesLabel1.Text = "Notes:";
            // 
            // notesTextBox
            // 
            this.notesTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.addressesBindingSource, "Notes", true));
            this.notesTextBox.Location = new System.Drawing.Point(45, 205);
            this.notesTextBox.Multiline = true;
            this.notesTextBox.Name = "notesTextBox";
            this.notesTextBox.Size = new System.Drawing.Size(427, 124);
            this.notesTextBox.TabIndex = 18;
            // 
            // savenotetoolStripButton
            // 
            this.savenotetoolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.savenotetoolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("savenotetoolStripButton.Image")));
            this.savenotetoolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.savenotetoolStripButton.Name = "savenotetoolStripButton";
            this.savenotetoolStripButton.Size = new System.Drawing.Size(23, 22);
            this.savenotetoolStripButton.Text = "toolStripButton1";
            this.savenotetoolStripButton.Click += new System.EventHandler(this.savenotetoolStripButton_Click);
            // 
            // Notes
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(515, 381);
            this.Controls.Add(notesLabel1);
            this.Controls.Add(this.notesTextBox);
            this.Controls.Add(this.bindingNavigator1);
            this.Controls.Add(countryLabel);
            this.Controls.Add(this.countryLabel1);
            this.Controls.Add(zip_Postal_CodeLabel);
            this.Controls.Add(this.zip_Postal_CodeLabel1);
            this.Controls.Add(state_ProvinceLabel);
            this.Controls.Add(this.state_ProvinceLabel1);
            this.Controls.Add(cityLabel);
            this.Controls.Add(this.cityLabel1);
            this.Controls.Add(addressLabel);
            this.Controls.Add(this.addressLabel1);
            this.Controls.Add(lastNameLabel);
            this.Controls.Add(this.lastNameLabel1);
            this.Controls.Add(firstNameLabel);
            this.Controls.Add(this.firstNameLabel1);
            this.Name = "Notes";
            this.Text = "Notes";
            this.Load += new System.EventHandler(this.Notes_Load);
            ((System.ComponentModel.ISupportInitialize)(this.notesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressesDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.addressesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingNavigator1)).EndInit();
            this.bindingNavigator1.ResumeLayout(false);
            this.bindingNavigator1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private addressesDataSet addressesDataSet;
        private System.Windows.Forms.BindingSource notesBindingSource;
        private System.Windows.Forms.BindingSource addressesBindingSource;
        private addressesDataSetTableAdapters.addressesTableAdapter addressesTableAdapter;
        private addressesDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.Label firstNameLabel1;
        private System.Windows.Forms.Label lastNameLabel1;
        private System.Windows.Forms.Label addressLabel1;
        private System.Windows.Forms.Label cityLabel1;
        private System.Windows.Forms.Label state_ProvinceLabel1;
        private System.Windows.Forms.Label zip_Postal_CodeLabel1;
        private System.Windows.Forms.Label countryLabel1;
        private System.Windows.Forms.BindingNavigator bindingNavigator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.TextBox notesTextBox;
        private System.Windows.Forms.ToolStripButton savenotetoolStripButton;
    }
}